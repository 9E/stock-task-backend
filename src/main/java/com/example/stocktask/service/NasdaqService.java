package com.example.stocktask.service;

import com.example.stocktask.mapper.StockDataMapper;
import com.example.stocktask.model.StockDataDto;
import com.example.stocktask.model.external.RealtimeDataResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.*;
import java.util.stream.IntStream;

import static com.example.stocktask.Constants.*;


@Service
public class NasdaqService {

    private StockDataMapper stockDataMapper;
    private RestTemplate restTemplate;
    private MovingAveragesService movingAveragesService;

    public NasdaqService(StockDataMapper stockDataMapper, RestTemplate restTemplate,
                         MovingAveragesService movingAveragesService) {
        this.stockDataMapper = stockDataMapper;
        this.restTemplate = restTemplate;
        this.movingAveragesService = movingAveragesService;
    }

    @Cacheable("realtimeData")
    public List<StockDataDto> getRealtimeData(String resource, Optional<String> startDate, Optional<String> endDate) {
        MultiValueMap<String, String> queryParams = buildQueryParams(startDate, endDate);
        URI uri = buildUri(resource, queryParams);

        RealtimeDataResponse realtimeDataResponse = restTemplate.getForEntity(uri, RealtimeDataResponse.class).getBody();
        List<StockDataDto> stockDataDtoList = stockDataMapper.stockDataToDto(realtimeDataResponse.getDataset().getData());

        stockDataDtoList.sort(Comparator.comparing(StockDataDto::getDate));
        setMovingAverages(stockDataDtoList);
        return stockDataDtoList;
    }

    private void setMovingAverages(List<StockDataDto> stockDataDtoList) {
        List<Double> fiveDayMovingAverages = movingAveragesService.calculateMovingAverages(stockDataDtoList, 5);
        List<Double> twentyDayMovingAverages = movingAveragesService.calculateMovingAverages(stockDataDtoList, 20);

        IntStream.range(0, stockDataDtoList.size())
                .forEach(i -> {
                    stockDataDtoList.get(i).setFiveDayMovingAverage(fiveDayMovingAverages.get(i));
                    stockDataDtoList.get(i).setTwentyDayMovingAverage(twentyDayMovingAverages.get(i));
                });
    }

    private URI buildUri(String resource, MultiValueMap<String, String> queryParams) {
        String url = API_URL + resource + "." + RESPONSE_TYPE;
        URI uri = UriComponentsBuilder.fromUriString(url)
                .queryParams(queryParams)
                .build()
                .toUri();
        return uri;
    }

    private MultiValueMap<String, String> buildQueryParams(Optional<String> startDate, Optional<String> endDate) {
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.add(API_KEY_NAME, API_KEY);
        startDate.ifPresent(value -> queryParams.add(START_DATE, value));
        endDate.ifPresent(value -> queryParams.add(END_DATE, value));
        return queryParams;
    }
}

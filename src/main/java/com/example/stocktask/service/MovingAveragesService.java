package com.example.stocktask.service;

import com.example.stocktask.model.StockDataDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovingAveragesService {

    public List<Double> calculateMovingAverages(List<StockDataDto> stockDataDtoList, int interval) {
        Double sumOfLastelements = 0d;
        List<Double> movingAverages = new ArrayList<>();
        for (int i = 0; i < stockDataDtoList.size(); i++) {
            sumOfLastelements += stockDataDtoList.get(i).getNumberOfShares();
            if (i > interval - 2) {
                movingAverages.add(sumOfLastelements / interval);
                sumOfLastelements -= stockDataDtoList.get(i - interval + 1).getNumberOfShares();
            } else {
                movingAverages.add(null);
            }
        }
        return movingAverages;
    }
}

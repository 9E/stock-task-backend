package com.example.stocktask.model.external;

import java.util.List;

public class Dataset {

    List<List<Object>> data;

    public List<List<Object>> getData() {
        return data;
    }

    public void setData(List<List<Object>> data) {
        this.data = data;
    }
}

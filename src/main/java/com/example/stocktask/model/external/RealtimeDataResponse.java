package com.example.stocktask.model.external;

public class RealtimeDataResponse {
    Dataset dataset;

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }
}

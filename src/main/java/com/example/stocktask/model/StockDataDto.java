package com.example.stocktask.model;

import java.sql.Timestamp;

public class StockDataDto {
    private Timestamp date;
    private Double open;
    private Double high;
    private Double low;
    private Double close;
    private Double numberOfShares;
    private Double fiveDayMovingAverage;
    private Double twentyDayMovingAverage;

    public StockDataDto() {}
    public StockDataDto(Double numberOfShares) {
        this.numberOfShares = numberOfShares;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public Double getNumberOfShares() {
        return numberOfShares;
    }

    public void setNumberOfShares(Double numberOfShares) {
        this.numberOfShares = numberOfShares;
    }

    public Double getFiveDayMovingAverage() {
        return fiveDayMovingAverage;
    }

    public void setFiveDayMovingAverage(Double fiveDayMovingAverage) {
        this.fiveDayMovingAverage = fiveDayMovingAverage;
    }

    public Double getTwentyDayMovingAverage() {
        return twentyDayMovingAverage;
    }

    public void setTwentyDayMovingAverage(Double twentyDayMovingAverage) {
        this.twentyDayMovingAverage = twentyDayMovingAverage;
    }
}

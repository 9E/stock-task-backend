package com.example.stocktask.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class FailedToProcessDataException extends RuntimeException {

    public FailedToProcessDataException(String message) {
        super(message);
    }
}

package com.example.stocktask.controller;

import com.example.stocktask.model.StockDataDto;
import com.example.stocktask.service.NasdaqService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static com.example.stocktask.Constants.END_DATE;
import static com.example.stocktask.Constants.START_DATE;

@RestController
@RequestMapping("/api/v1/nasdaq-controller")
public class NasdaqController {
    private NasdaqService nasdaqService;

    public NasdaqController(NasdaqService nasdaqService) {
        this.nasdaqService = nasdaqService;
    }

    @GetMapping("/{resource}")
    public ResponseEntity<List<StockDataDto>> getDateVolumes(@PathVariable String resource,
                                                             @RequestParam(START_DATE) Optional<String> startDate,
                                                             @RequestParam(END_DATE) Optional<String> endDate) {
        return ResponseEntity.ok(nasdaqService.getRealtimeData(resource, startDate, endDate));
    }
}

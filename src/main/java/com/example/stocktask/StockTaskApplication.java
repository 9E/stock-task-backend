package com.example.stocktask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockTaskApplication.class, args);
	}

}

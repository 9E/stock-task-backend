package com.example.stocktask.mapper;

import com.example.stocktask.exception.FailedToProcessDataException;
import com.example.stocktask.model.StockDataDto;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class StockDataMapper {

    public List<StockDataDto> stockDataToDto(List<List<Object>> externalResponseData) {
        return externalResponseData.stream()
                .map(data -> {
                    StockDataDto stockDataDto = new StockDataDto();
                    stockDataDto.setDate(stringToTimestamp((String) data.get(0)));
                    stockDataDto.setOpen((Double) data.get(1));
                    stockDataDto.setHigh((Double) data.get(2));
                    stockDataDto.setLow((Double) data.get(3));
                    stockDataDto.setClose((Double) data.get(4));
                    stockDataDto.setNumberOfShares((Double) data.get(6));
                    return stockDataDto;
                }).collect(Collectors.toCollection(ArrayList::new));
    }

    private Timestamp stringToTimestamp(String stringDate) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date parsedDate = formatter.parse(stringDate);
            return new Timestamp(parsedDate.getTime());
        } catch (ParseException e) {
            throw new FailedToProcessDataException("Error while processing data internally");
        }
    }
}

package com.example.stocktask.service;

import com.example.stocktask.model.StockDataDto;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MovingAveragesServiceTest {
    MovingAveragesService movingAveragesService = new MovingAveragesService();

    @Test
    void shouldCalculateMovingAverages() {
        // given
        List<StockDataDto> externalResponseData = buildStockDataDtoList();

        // when
        List<Double> threeDayMovingAverages = movingAveragesService.calculateMovingAverages(externalResponseData, 3);
        List<Double> fiveDayMovingAverages = movingAveragesService.calculateMovingAverages(externalResponseData, 5);

        // then
        assertNull(threeDayMovingAverages.get(1));
        assertEquals(2d, threeDayMovingAverages.get(2));
        assertEquals(9d, threeDayMovingAverages.get(9));

        assertNull(fiveDayMovingAverages.get(3));
        assertEquals(3d, fiveDayMovingAverages.get(4));
        assertEquals(8d, fiveDayMovingAverages.get(9));
    }

    private List<StockDataDto> buildStockDataDtoList() {
        return Arrays.asList(
                new StockDataDto(1d),
                new StockDataDto(2d),
                new StockDataDto(3d),
                new StockDataDto(4d),
                new StockDataDto(5d),
                new StockDataDto(6d),
                new StockDataDto(7d),
                new StockDataDto(8d),
                new StockDataDto(9d),
                new StockDataDto(10d)
        );
    }
}
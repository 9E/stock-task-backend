package com.example.stocktask.mapper;

import com.example.stocktask.exception.FailedToProcessDataException;
import com.example.stocktask.model.StockDataDto;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StockDataMapperTest {
    private StockDataMapper stockDataMapper = new StockDataMapper();

    @Test
    void shouldMapStockDataToDto() {
        // given
        List<Object> expectedList = List.of("2012-12-12", 1d, 2d, 3d, 4d, 5d, 6d, 7d, 8d);
        List<List<Object>> externalResponseData = List.of(expectedList);

        // when
        List<StockDataDto> stockDataDtoList = stockDataMapper.stockDataToDto(externalResponseData);
        StockDataDto actualStockDataDto = stockDataDtoList.get(0);

        // then
        assertEquals(1, stockDataDtoList.size());
        assertEquals(Timestamp.valueOf("2012-12-12 0:00:00"), actualStockDataDto.getDate());
        assertEquals(expectedList.get(1), actualStockDataDto.getOpen());
        assertEquals(expectedList.get(2), actualStockDataDto.getHigh());
        assertEquals(expectedList.get(3), actualStockDataDto.getLow());
        assertEquals(expectedList.get(4), actualStockDataDto.getClose());
        assertEquals(expectedList.get(6), actualStockDataDto.getNumberOfShares());
    }

    @Test
    void shouldNotMapStockDataToDto() {
        // given
        List<List<Object>> externalResponseData1 = List.of(List.of("123", 1d, 2d, 3d, 4d, 5d, 6d, 7d, 8d));
        List<List<Object>> externalResponseData2 = List.of(List.of("2012-12-12", 1d, 2d, 3d, 4d, 5d));

        // then
        assertThrows(FailedToProcessDataException.class, () -> stockDataMapper.stockDataToDto(externalResponseData1));
        assertThrows(IndexOutOfBoundsException.class, () -> stockDataMapper.stockDataToDto(externalResponseData2));
    }
}